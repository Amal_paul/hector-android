/*******************************************************************************
 * Copyright (c) 1999, 2014 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution. 
 *
 * The Eclipse Public License is available at 
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at 
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 */
package com.hector.android.mqtt;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.hector.android.activities.ActivitiesActivity;
import com.hector.android.application.Hector;
import com.hector.android.notifications.NotificationServiceManager;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * Handles call backs from the MQTT Client
 *
 */
public class MqttCallbackHandler implements MqttCallback {
    private static final String LOG_TAG = "MqttCallbackHandler";
  /** {@link Context} for the application used to format and import external strings**/
  private Context context;
  /** Client handle to reference the connection that this handler is attached to**/
  private String clientHandle;

  /**
   * Creates an <code>MqttCallbackHandler</code> object
   * @param context The application's context
   * @param clientHandle The handle to a {@link MqttConnection} object
   */
  public MqttCallbackHandler(Context context, String clientHandle)
  {
    this.context = context;
    this.clientHandle = clientHandle;
  }

  /**
   * @see org.eclipse.paho.client.mqttv3.MqttCallback#connectionLost(java.lang.Throwable)
   */
  @Override
  public void connectionLost(Throwable cause) {
//	  cause.printStackTrace();
    if (cause != null) {
      Log.i(LOG_TAG, "connection Lost " + cause.toString());
    }
  }

  /**
   * @see org.eclipse.paho.client.mqttv3.MqttCallback#messageArrived(java.lang.String, org.eclipse.paho.client.mqttv3.MqttMessage)
   */
  @Override
  public void messageArrived(String topic, MqttMessage message) throws Exception {
      Log.i(LOG_TAG, "Topic " + topic + " Message " + new String(message.getPayload()));
      JsonObject jsonObject = new Gson().fromJson(new String(message.getPayload()), JsonObject.class);
      JsonObject patientObject = jsonObject.getAsJsonObject("actor");
      String actorId = patientObject.get("id").getAsString();
        if(ActivitiesActivity.isScreenLive && Hector.currentPatientId.equals(actorId)){
            Intent newMessageIntent = new Intent(MqttConstants.NEW_ACTIVITY_MESSAGE_RECEIVED + ActivitiesActivity.ACTIVITY_TYPE_ALL);
            newMessageIntent.putExtra(MqttConstants.NEW_ACTIVITY_MESSAGE_RECEIVED, new String(message.getPayload()));
            LocalBroadcastManager.getInstance(Hector.applicationContext).sendBroadcast(newMessageIntent);
        } else {
            String actorName = patientObject.get("identifier").getAsString();
            NotificationServiceManager.showNewActivityNotification(actorId, actorName);
        }

  }

  /**
   * @see org.eclipse.paho.client.mqttv3.MqttCallback#deliveryComplete(org.eclipse.paho.client.mqttv3.IMqttDeliveryToken)
   */
  @Override
  public void deliveryComplete(IMqttDeliveryToken token) {
    // Do nothing
  }

}
