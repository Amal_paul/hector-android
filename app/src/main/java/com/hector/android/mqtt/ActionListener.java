/*******************************************************************************
 * Copyright (c) 1999, 2014 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution. 
 *
 * The Eclipse Public License is available at 
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at 
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 */
package com.hector.android.mqtt;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.hector.android.application.Hector;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

class ActionListener implements IMqttActionListener {

  private static final String LOG_TAG = "ActionListener";
//  private static ActionListener mActionListener;

  /**
   * Actions that can be performed Asynchronously <strong>and</strong> associated with a
   * {@link ActionListener} object
   * 
   */
  enum Action {
    /** Connect Action **/
    CONNECT,
    /** Disconnect Action **/
    DISCONNECT,
    /** Subscribe Action **/
    SUBSCRIBE,
    /** Publish Action **/
    PUBLISH
  }

  /**
   * The {@link Action} that is associated with this instance of
   * <code>ActionListener</code>
   **/
  private Action action;
  /** The arguments passed to be used for formatting strings**/
  private String[] additionalArgs;
  /** Handle of the {@link MqttConnection} this action was being executed on **/
  private String clientHandle;
  /** {@link Context} for performing various operations **/
  private Context context;

  /**
   * Creates a generic action listener for actions performed form any activity
   * 
   * @param context
   *            The application context
   * @param action
   *            The action that is being performed
   * @param clientHandle
   *            The handle for the client which the action is being performed
   *            on
   * @param additionalArgs
   *            Used for as arguments for string formating
   */
  private ActionListener(Context context, Action action,
                        String clientHandle, String... additionalArgs) {
    this.context = context;
    this.action = action;
    this.clientHandle = clientHandle;
    this.additionalArgs = additionalArgs;
  }

  private ActionListener(Context context, Action action) {
    this.context = context;
    this.action = action;
//    this.clientHandle = clientHandle;
    this.additionalArgs = additionalArgs;
  }

  public static ActionListener getActionListenerInstance(Context context, Action action){
//    if(mActionListener == null) {
//        mActionListener = new ActionListener(context, action);
//    }
    return new ActionListener(context, action);
  }

  /**
   * The action associated with this listener has been successful.
   * 
   * @param asyncActionToken
   *            This argument is not used
   */
  @Override
  public void onSuccess(IMqttToken asyncActionToken) {
    switch (action) {
      case CONNECT :
        connect();
        break;
      case DISCONNECT :
        disconnect();
        break;
      case SUBSCRIBE :
        subscribe();
        break;
      case PUBLISH :
        publish();
        break;
    }

  }

  /**
   * A publish action has been successfully completed, update connection
   * object associated with the client this action belongs to, then notify the
   * user of success
   */
  private void publish() {
    Log.i(LOG_TAG, "Publish successful");
    Intent intent = new Intent(MqttConstants.PUBLISH_STATUS_UPDATE);
    intent.putExtra(MqttConstants.PUBLISH_STATUS_UPDATE, MqttConstants.PUBLISH_SUCCESS);
    LocalBroadcastManager.getInstance(Hector.applicationContext).sendBroadcast(intent);
  }

  /**
   * A subscribe action has been successfully completed, update the connection
   * object associated with the client this action belongs to and then notify
   * the user of success
   */
  private void subscribe() {
    Log.i(LOG_TAG, "Subscribe successful");
  }

  /**
   * A disconnection action has been successfully completed, update the
   * connection object associated with the client this action belongs to and
   * then notify the user of success.
   */
  private void disconnect() {
    Log.i(LOG_TAG, "Disconnected successfully");
  }

  /**
   * A connection action has been successfully completed, update the
   * connection object associated with the client this action belongs to and
   * then notify the user of success.
   */
  private void connect() {
    Log.i(LOG_TAG, "MqttConnection successful");
    Intent intent = new Intent(MqttConstants.CONNECTION_STATUS_UPDATE);
    intent.putExtra(MqttConstants.CONNECTION_STATUS_UPDATE, MqttConstants.CONNECTION_SUCCESS);
    LocalBroadcastManager.getInstance(Hector.applicationContext).sendBroadcast(intent);
  }

  /**
   * The action associated with the object was a failure
   * 
   * @param token
   *            This argument is not used
   * @param exception
   *            The exception which indicates why the action failed
   */
  @Override
  public void onFailure(IMqttToken token, Throwable exception) {
    switch (action) {
      case CONNECT :
        connect(exception);
        break;
      case DISCONNECT :
        disconnect(exception);
        break;
      case SUBSCRIBE :
        subscribe(exception);
        break;
      case PUBLISH :
        publish(exception);
        break;
    }

  }

  /**
   * A publish action was unsuccessful, notify user and update client history
   * 
   * @param exception
   *            This argument is not used
   */
  private void publish(Throwable exception) {
    Log.i(LOG_TAG, "Publish failed");
    Intent intent = new Intent(MqttConstants.PUBLISH_STATUS_UPDATE);
    intent.putExtra(MqttConstants.PUBLISH_STATUS_UPDATE, MqttConstants.PUBLISH_FAILURE);
    LocalBroadcastManager.getInstance(Hector.applicationContext).sendBroadcast(intent);
  }

  /**
   * A subscribe action was unsuccessful, notify user and update client history
   * @param exception This argument is not used
   */
  private void subscribe(Throwable exception) {
    Log.i(LOG_TAG, "Subscribe failed");
  }

  /**
   * A disconnect action was unsuccessful, notify user and update client history
   * @param exception This argument is not used
   */
  private void disconnect(Throwable exception) {
    Log.i(LOG_TAG, "Disconnect failed");
  }

  /**
   * A connect action was unsuccessful, notify the user and update client history
   * @param exception This argument is not used
   */
  private void connect(Throwable exception) {
    Log.i(LOG_TAG, "MqttConnection failed");
    Intent intent = new Intent(MqttConstants.CONNECTION_STATUS_UPDATE);
    intent.putExtra(MqttConstants.CONNECTION_STATUS_UPDATE, MqttConstants.CONNECTION_FAILURE);
    LocalBroadcastManager.getInstance(Hector.applicationContext).sendBroadcast(intent);
  }

}