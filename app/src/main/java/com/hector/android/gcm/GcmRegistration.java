package com.hector.android.gcm;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.hector.android.R;
import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.BaseHttpResponse;
import com.hector.android.pojo.responses.ErrorHttpResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Arjun on 9/12/15.
 */
public class GcmRegistration {
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String LOG_TAG = "GcmRegistration";
    private static GoogleCloudMessaging gcm;
    private static String regId;
    private Context context;

    public GcmRegistration(Context context) {
        this.context = context;
    }

    public static GcmRegistration getInstance(Context context) {
        return new GcmRegistration(context);
    }

    public void getAndSendGcmRegId() {
        if (checkGooglePlayServices()) {
            regId = getRegistrationId();
            Log.d(LOG_TAG,"GCM ID "+regId);
            if (regId.isEmpty()) {
                registerInBackground();
            }
        } else {
            Log.i("Play services", "No valid Google Play Services APK found.");
        }
    }

    private boolean checkGooglePlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, (AppCompatActivity) context,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("Play services error", "This device is not supported.");
                ((AppCompatActivity) context).finish();
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId() {
        //        if (registrationId.isEmpty()) {
//            Log.i("GCM", "Registration not found in shared prefs.");
//            return "";
//        }
        return SharedPreferencesManager
                .getStringPreference(SharedPreferencesManager.PROPERTY_GCM_REG_ID, "");
    }

    private void registerInBackground() {
        new AsyncTask() {
            @Override
            protected String doInBackground(Object[] params) {
                String msg;
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(Hector.applicationContext);
                    }
                    InstanceID instanceID = InstanceID.getInstance(Hector.applicationContext);
                    regId = instanceID.getToken(context.getString(R.string.gcm_sender_id),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    storeRegistrationId();
                    msg = "Device registered, registration ID=" + regId;
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }
        }.execute(null, null, null);
    }

    private void storeRegistrationId() {
        Map<String, String> gmcIdMap = new HashMap<>();
        gmcIdMap.put("deviceToken", regId);
        gmcIdMap.put("deviceType", "android");
        MyHttp.getInstance(Hector.applicationContext, Endpoints.getGcmRegisterUrl(SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_ID, "")), MyHttp.POST, BaseHttpResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .addJson(gmcIdMap)
                .send(new MyHttpCallback<ErrorHttpResponse>() {
                    @Override
                    public void success(ErrorHttpResponse data) {

                        if (data == null) {
                            SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.PROPERTY_GCM_REG_ID, regId);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Log.i(LOG_TAG, "Gcm reg failed");
                    }

                    @Override
                    public void onBefore() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }
}
