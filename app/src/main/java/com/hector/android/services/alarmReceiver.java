package com.hector.android.services;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;

/**
 * Created by amal on 04/03/16.
 */
public class alarmReceiver extends BroadcastReceiver {
    public static MediaPlayer mp;

    @Override
    public void onReceive(Context context, Intent intent) {
        Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        mp = MediaPlayer.create(context, alert);
        mp.setLooping(true);
        mp.start();

    }
}
