package com.hector.android.libs;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;

import com.hector.android.R;


/**
 * Created by arjun on 10/13/15.
 */
public class SettingWithSwitch extends RelativeLayout {
    TypeFaceTextView settingTitle;
    TypeFaceTextView settingState;
    SwitchCompat toggleSwitch;

    public SettingWithSwitch(Context context) {
        super(context);
        init(context, null, 0, 0);
    }

    public SettingWithSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0, 0);
    }

    public SettingWithSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public SettingWithSwitch(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attributeSet, int defStyleAttr, int defStyleRes){
        LayoutInflater.from(context).inflate(R.layout.settings_item_with_switch, this, true);
        TypedArray values = context.getTheme().obtainStyledAttributes(attributeSet
                , R.styleable.SettingWithSwitch
                , defStyleAttr
                , defStyleRes);
        settingTitle = (TypeFaceTextView) findViewById(R.id.setting_item_title);
        settingState = (TypeFaceTextView) findViewById(R.id.setting_item_detail);
        toggleSwitch = (SwitchCompat) findViewById(R.id.setting_item_switch);
        String title;
        String detail;
        boolean value;
        boolean switchVisible;
        Integer background;
        try {
            title = values.getString(R.styleable.SettingWithSwitch_settingTitle);
            detail = values.getString(R.styleable.SettingWithSwitch_settingDetail);
            value = values.getBoolean(R.styleable.SettingWithSwitch_value, true);
            switchVisible = values.getBoolean(R.styleable.SettingWithSwitch_switchVisible, true);
            background = values.getResourceId(R.styleable.SettingWithSwitch_bg, -1);
        } finally {
            values.recycle();
        }
        settingTitle.setText(title);
        toggleSwitch.setChecked(value);
        if(switchVisible){
            toggleSwitch.setVisibility(VISIBLE);
        }
        if(detail == null){
            settingState.setVisibility(GONE);
        } else {
            settingState.setText(detail);
        }
        setBackgroundResource(background);
    }

    public boolean isChecked(){
        return this.toggleSwitch.isChecked();
    }

    public void setChecked(boolean value){
        this.toggleSwitch.setChecked(value);
    }

    public void setTitle(String title){
        this.settingTitle.setText(title);
    }

    public void setTitle(int resId){
        this.settingTitle.setText(resId);
    }

    public void setDetail(String detail){
        this.settingState.setText(detail);
    }

    public void setDetail(int resId){
        this.settingState.setText(resId);
    }

    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener onCheckedChangeListener){
        this.toggleSwitch.setOnCheckedChangeListener(onCheckedChangeListener);
    }
}
