package com.hector.android.libs.recycleriewanimator;

import android.view.View;

public interface RecyclerViewItemOnScrollAnimator {
    void onAnimateViewHolder(View view, int position);
    void onPrepareToAnimateViewHolder(View view, int position);
}
