package com.hector.android.libs.buttonwithprogressbar;

interface OnAnimationEndListener {

    void onAnimationEnd();
}
