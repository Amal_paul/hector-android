package com.hector.android.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.PointTarget;
import com.hector.android.R;
import com.hector.android.application.DialogClickListener;
import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.SmartCommFsblConfigDetails;
import com.hector.android.pojo.requests.DeviceDetailsRequest;
import com.hector.android.pojo.responses.pateintdevicedetails.Device;
import com.hector.android.pojo.responses.pateintdevicedetails.DeviceConfigResponse;
import com.hector.android.pojo.responses.pateintdevicedetails.DeviceDetailsResponse;
import com.hector.android.pojo.responses.pateintdevicedetails.PatientBlisterResponse;

/**
 * Created by arjun on 12/15/15.
 */
public class DeviceConfigurationFragment extends Fragment {
    private View mRootView;
    private TypeFaceTextView wifiConfigView;
    private TypeFaceTextView fsblConfigView;
    private ShowcaseView wifiConfigShowcaseview;

    private String FSBL_ID = "", SMART_CONNECTOR_ID = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_device_configration, container, false);
        initialiseViews();
        bindEvents();
        if (!SharedPreferencesManager.getBooleanPreference(SharedPreferencesManager.wifiConfig_showcase, false)) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int[] location = new int[2];
                    wifiConfigView.getLocationInWindow(location);
                    final int x = location[0] + 100;
                    final int y = location[1] + wifiConfigView.getHeight() / 2;
                    wifiConfigShowcaseview = new ShowcaseView.Builder(getActivity())
                            .withMaterialShowcase()
                            .setTarget(new PointTarget(x, y))
                            .setContentTitle("Your Smart-connector's")
                            .setContentText("wifi needs to be configured, for it to work." +
                                    System.getProperty("line.separator") + "Let's do it." +
                                    System.getProperty("line.separator") + System.getProperty("line.separator") + "(Click allow if prompted)")
                            .setStyle(R.style.CustomShowcaseTheme2)
                            .replaceEndButton(R.layout.view_custom_button)
                            .setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            })
                            .build();

                }
            }, 400);
        }
        return mRootView;
    }

    private void initialiseViews() {
        wifiConfigView = (TypeFaceTextView) mRootView.findViewById(R.id.wifi_config_link);
        fsblConfigView = (TypeFaceTextView) mRootView.findViewById(R.id.fsbl_config_link);
    }

    private void bindEvents() {
        wifiConfigView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wifiConfigShowcaseview != null && wifiConfigShowcaseview.isShowing()) {
                    wifiConfigShowcaseview.hide();
                    SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.wifiConfig_showcase, true);
                }
                getSmartConnectId();
            }
        });
        fsblConfigView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceId();
            }
        });
    }

    private void getSmartConnectId() {
        new DeviceDetailsRequest().getDevicesOfPatients(getActivity(), new MyHttpCallback<DeviceDetailsResponse>() {
            @Override
            public void success(DeviceDetailsResponse data) {
                Utils.dismissLoadingDialog();
                if (data.code == null) {
                    for (int j = 0; j < data.device.size(); j++) {
                        if (data.device.get(j).deviceType == null) {
                            return;
                        }
                        if (data.device.get(j).deviceType.equals(Device.DEVICE_TYPE_SMART_CONNECTOR)) {
                            if (data.device.get(j).deviceId != null || !data.device.get(j).deviceId.isEmpty()) {
                                if (data.device.get(j).isAssigned == true)
                                    SMART_CONNECTOR_ID = data.device.get(j).deviceId;
                            }
                        }

                    }
                    //  checkForConfiguration(data.device.deviceId);
                    if (SMART_CONNECTOR_ID.isEmpty()) {
                        // show alert dialog
                        showInfoDialog(getResources().getString(R.string.device_not_found), getResources().getString(R.string.device_not_found_detail));
                    } else {
                        WifiConfigurationFragment wifiConfigurationFragment = new WifiConfigurationFragment();
                        Bundle args = new Bundle();
                        args.putString(Device.DEVICE_TYPE_SMART_CONNECTOR, SMART_CONNECTOR_ID);
                        wifiConfigurationFragment.setArguments(args);
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .addToBackStack("DeviceConfig")
                                .replace(R.id.device_config_container, wifiConfigurationFragment)
                                .commit();
                    }
                } else {
                    Utils.dismissLoadingDialog();
                    showInfoDialog(getResources().getString(R.string.device_not_found), getResources().getString(R.string.device_not_found_detail));
                }
            }

            @Override
            public void failure(String msg) {
                Toast.makeText(getActivity(), getResources().getString(R.string.http_error), Toast.LENGTH_LONG).show();
                Utils.dismissLoadingDialog();
            }

            @Override
            public void onBefore() {
                Utils.showLoadingDialog(getActivity(), getResources().getString(R.string.fetching_data), false);
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void getDeviceId() {
        new DeviceDetailsRequest().getDevicesOfPatients(getActivity(), new MyHttpCallback<DeviceDetailsResponse>() {
            @Override
            public void success(DeviceDetailsResponse data) {
                Utils.dismissLoadingDialog();
                if (data.code == null) {
                    for (int j = 0; j < data.device.size(); j++) {
                        if (data.device.get(j).deviceType == null) {
                            return;
                        }
                        if (data.device.get(j).deviceType.equals(Device.DEVICE_TYPE_SMART_CONNECTOR)) {
                            if (data.device.get(j).isAssigned) {
                                if (data.device.get(j).deviceId != null || !data.device.get(j).deviceId.isEmpty()) {
                                    SMART_CONNECTOR_ID = data.device.get(j).deviceId;
                                }
                            }
                        }
                        if (data.device.get(j).deviceType.equals(Device.DEVICE_TYPE_FSBL)) {
                            if (data.device.get(j).isAssigned) {
                                if (data.device.get(j).deviceId != null || !data.device.get(j).deviceId.isEmpty()) {
                                    FSBL_ID = data.device.get(j).deviceId;
                                }
                            }
                        }
                    }
                    //  checkForConfiguration(data.device.deviceId);
                    if (SMART_CONNECTOR_ID.isEmpty()) {
                        // show alert dialog
                        showInfoDialog(getResources().getString(R.string.device_not_found), getResources().getString(R.string.device_not_found_detail));
                    } else {
                        if (FSBL_ID.isEmpty()) {
                            // show alert dialog
                            showInfoDialog(getResources().getString(R.string.device_not_found), getResources().getString(R.string.device_fsbl_not_found_detail));
                        } else {
                            showConfigureDialog(SMART_CONNECTOR_ID, FSBL_ID);
                        }
                    }
                } else {
                    Utils.dismissLoadingDialog();
                    showInfoDialog(getResources().getString(R.string.device_not_found), getResources().getString(R.string.device_not_found_detail));
                }
            }

            @Override
            public void failure(String msg) {
                Toast.makeText(getActivity(), getResources().getString(R.string.http_error), Toast.LENGTH_LONG).show();
                Utils.dismissLoadingDialog();
            }

            @Override
            public void onBefore() {
                Utils.showLoadingDialog(getActivity(), getResources().getString(R.string.fetching_data), false);
            }

            @Override
            public void onFinish() {
            }
        });
    }

    private void showInfoDialog(String title, String textToShow) {
        Utils.showAlertDialog(getActivity(), title,
                textToShow, true, getResources().getString(R.string.ok),
                getResources().getString(R.string.cancel), new DialogClickListener() {
                    @Override
                    public void onPositiveButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClicked(AlertDialog alertDialog) {
                    }

                    @Override
                    public void onDialogDismissed(AlertDialog alertDialog) {
                    }
                });
    }

    private void checkForConfiguration(final String deviceId) {
        MyHttp.getInstance(getActivity(), Endpoints.getAssociatedFsbl(deviceId), MyHttp.GET, PatientBlisterResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<PatientBlisterResponse>() {
                    @Override
                    public void success(PatientBlisterResponse data) {
                        Utils.dismissLoadingDialog();
                        if (data.code == MyHttp.REQUEST_OK) {
                            if (!data.body.isConnected) {
                                showConfigureDialog(deviceId, data.body.blisterId);
                            } else {
                                showInfoDialog(getResources().getString(R.string.no_new_blister), getResources().getString(R.string.no_new_blister_detail));
                            }
                        } else {
                            showInfoDialog(getResources().getString(R.string.device_not_found), getResources().getString(R.string.device_not_found_detail));
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Utils.dismissLoadingDialog();
                        if (getActivity() != null) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.http_error), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onBefore() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }

    private void showConfigureDialog(final String deviceId, final String blisterId) {
        Utils.showAlertDialog(getActivity(), "Please note",
                "The system is designed to configure your FSBL automatically." + System.getProperty("line.separator") +
                        "This option manually overrides automatic settings." + System.getProperty("line.separator") +
                        "Configured FSBL: " + FSBL_ID, false, "Re-configure?",
                getResources().getString(R.string.cancel), new DialogClickListener() {
                    @Override
                    public void onPositiveButtonClicked(AlertDialog alertDialog) {
                        sendDetails(deviceId, blisterId);
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onDialogDismissed(AlertDialog alertDialog) {
                    }
                });
    }

    private void sendDetails(String deviceId, String blisterId) {
        DeviceDetailsRequest configureRequest = new DeviceDetailsRequest();
        configureRequest.smartCommFsblConfigDetails = new SmartCommFsblConfigDetails();
        configureRequest.smartCommFsblConfigDetails.fsblId = blisterId;
        configureRequest.smartCommFsblConfigDetails.communicatorId = deviceId;
        configureRequest.configureDeviceWithFsbl(getActivity(), new MyHttpCallback<DeviceConfigResponse>() {
            @Override
            public void success(DeviceConfigResponse data) {
                if (data == null) {
                    showInfoDialog(getResources().getString(R.string.device_configured), getResources().getString(R.string.device_configure_detail));
                } else {
                    String msg = data.message == null || data.message.isEmpty() ? getResources().getString(R.string.http_error) : data.message;
                    Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                }

            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
            }

            @Override
            public void onBefore() {
                Utils.showLoadingDialog(getActivity(), "Please wait...", false);
            }

            @Override
            public void onFinish() {
                Utils.dismissLoadingDialog();
            }
        });
    }
}
