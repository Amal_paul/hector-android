package com.hector.android.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.PointTarget;
import com.hector.android.R;
import com.hector.android.activities.ActivitiesActivity;
import com.hector.android.activities.AlertActivity;
import com.hector.android.activities.DeviceConfigurationActivity;
import com.hector.android.activities.DevicesActivity;
import com.hector.android.activities.KarmaActivity;
import com.hector.android.activities.PreferenceActivity;
import com.hector.android.activities.SwitchPatientsActivity;
import com.hector.android.application.DialogClickListener;
import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.responses.BaseHttpResponse;
import com.hector.android.pojo.responses.ErrorHttpResponse;

import java.util.Set;

/**
 * Created by arjun on 11/15/15.
 */
public class SideNavigationDrawer extends Fragment {

    public DrawerLayout drawerLayout;
    private View rootView;
    private TypeFaceTextView settingsMenu;
    private TypeFaceTextView preferencesMenu;
    private TypeFaceTextView deviceConfigMenu;
    private TypeFaceTextView activitiesMenu;
    private TypeFaceTextView homeMenu;
    private TypeFaceTextView userNameView;
    private TypeFaceTextView switchPatienMenu;
    private TypeFaceTextView karmaMenu;
    private TypeFaceTextView todayAlert;
    private String myIndex = "";
    private boolean doUserHaveUnseenActivity = false;
    private Set<String> usersSet;
    private TypeFaceTextView logoutMenu;

    private ShowcaseView deviceConfig, activitiesShowcaseView;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras.getString("subject").equals(getActivity().getResources().getString(R.string.title_activity_device_configuration))) {
                int[] location = new int[2];
                deviceConfigMenu.getLocationInWindow(location);
                final int x = location[0] + 50;
                final int y = location[1] + deviceConfigMenu.getHeight() / 2;

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        deviceConfig = new ShowcaseView.Builder(getActivity())
                                .withMaterialShowcase()
                                .setTarget(new PointTarget(x, y))
                                .setContentTitle("Configure your devices")
                                .setContentText("for the app to function." + System.getProperty("line.separator")
                                        + System.getProperty("line.separator") + System.getProperty("line.separator")
                                        + System.getProperty("line.separator")
                                        + System.getProperty("line.separator") + System.getProperty("line.separator")
                                        + "(Please click it.)")
                                .setStyle(R.style.CustomShowcaseTheme2)
                                .replaceEndButton(R.layout.view_custom_button)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                })
                                .build();
                    }
                }, 200);


            } else if (extras.getString("subject").equals(getActivity().getResources().getString(R.string.activities))) {

                int[] location = new int[2];
                activitiesMenu.getLocationInWindow(location);
                final int x = location[0] + 130;
                final int y = location[1] + activitiesMenu.getHeight() / 2;

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String content = "This is where you will be notified of the daily activities." + System.getProperty("line.separator")
                                + "ie," + System.getProperty("line.separator") + "- Has medication been taken"
                                + System.getProperty("line.separator") + "- Was it taken on time?"
                                + System.getProperty("line.separator") + "- Any critical issues with the device";
                        activitiesShowcaseView = new ShowcaseView.Builder(getActivity())
                                .withMaterialShowcase()
                                .setTarget(new PointTarget(x, y))
                                .setContentText(content)
                                .setStyle(R.style.CustomShowcaseTheme2)
                                .replaceEndButton(R.layout.view_custom_button_normal)
                                .setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        activitiesShowcaseView.hide();
                                        SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.activities_showcase, true);
                                        activitiesMenu.performClick();
                                    }
                                })
                                .build();
                    }
                }, 200);

            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter("device-configuration-onboarding"));
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.sidenavigation_layout, container, false);
        getUserDetail();
        initialiseViews();
        bindEvents();

        return rootView;
    }

    private void getUserDetail() {
//        Log.i("MyDebug ", Hector.currentPatientId);
        usersSet = SharedPreferencesManager.getStringSetPreference(SharedPreferencesManager.USERS_WITH_UNSEEN_ACTIVITIES);
        Log.i("MyDebug ", "In fragment size" + usersSet.size());
        for (String anUser : usersSet) {
            if (anUser.equals(Hector.currentPatientId)) {
                Log.i("MyDebug ", "In loop " + anUser);
                doUserHaveUnseenActivity = true;
                myIndex = anUser;
                break;
            }
        }
    }

    private void bindEvents() {
        settingsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivityAfterDelay(DevicesActivity.class);
            }
        });
        preferencesMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivityAfterDelay(PreferenceActivity.class);
            }
        });
        deviceConfigMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deviceConfig != null)
                    if (deviceConfig.isShowing()) {
                        deviceConfig.hide();
                        SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.deviceConfig_showcase, true);
                    }
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivityAfterDelay(DeviceConfigurationActivity.class);
            }
        });
        activitiesMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activitiesShowcaseView != null && activitiesShowcaseView.isShowing()) {
                    activitiesShowcaseView.hide();
                    SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.activities_showcase, true);
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivityAfterDelay(ActivitiesActivity.class);
                if (doUserHaveUnseenActivity) {
                    usersSet.remove(myIndex);
                    activitiesMenu.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_activities, 0, 0, 0);
                    SharedPreferencesManager.setStringSetPreference(SharedPreferencesManager.USERS_WITH_UNSEEN_ACTIVITIES, usersSet);
                }
            }
        });
        homeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });
        switchPatienMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivityAfterDelay(SwitchPatientsActivity.class);
            }
        });
        karmaMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivityAfterDelay(KarmaActivity.class);
            }
        });

        todayAlert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(GravityCompat.START);
                startActivityAfterDelay(AlertActivity.class);
            }
        });

        logoutMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showConfirmationDialog();
            }
        });
    }

    private void startActivityAfterDelay(final Class activityClass) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent settingsIntent = new Intent(getActivity(), activityClass);
                startActivity(settingsIntent);
            }
        }, 200);
    }

    private void initialiseViews() {
        userNameView = (TypeFaceTextView) rootView.findViewById(R.id.sideNavUserName);
        userNameView.setText(Hector.currentPatientName);
        homeMenu = (TypeFaceTextView) rootView.findViewById(R.id.sideNavHomeLink);
        settingsMenu = (TypeFaceTextView) rootView.findViewById(R.id.sideNavSettingsLink);
        preferencesMenu = (TypeFaceTextView) rootView.findViewById(R.id.sideNavPreferencesLink);
        deviceConfigMenu = (TypeFaceTextView) rootView.findViewById(R.id.sideNavDeviceConfigLink);
        activitiesMenu = (TypeFaceTextView) rootView.findViewById(R.id.sideNavActivitiesLink);
        switchPatienMenu = (TypeFaceTextView) rootView.findViewById(R.id.sideNavSwitchPatientLink);
        karmaMenu = (TypeFaceTextView) rootView.findViewById(R.id.sideNavKarmaLink);
        logoutMenu = (TypeFaceTextView) rootView.findViewById(R.id.sideNavLogOutLink);
        todayAlert = (TypeFaceTextView) rootView.findViewById(R.id.sideNavAlertsLink);
        if (doUserHaveUnseenActivity) {
            Log.i("MyDebug ", "entered");
            activitiesMenu.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_activities, 0, R.drawable.red_dot, 0);
        }


    }

    private void showConfirmationDialog() {
        Utils.showAlertDialog(getActivity(), getResources().getString(R.string.are_you_sure),
                getResources().getString(R.string.logout_warning), false, getResources().getString(R.string.continue_task),
                getResources().getString(R.string.cancel), new DialogClickListener() {
                    @Override
                    public void onPositiveButtonClicked(AlertDialog alertDialog) {
                        deleteGcmTokenFromBackend();
                    }

                    @Override
                    public void onNegativeButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onDialogDismissed(AlertDialog alertDialog) {
                    }
                });
    }

    private void deleteGcmTokenFromBackend() {
        MyHttp.getInstance(Hector.applicationContext, Endpoints.getDeleteGcmUrl(SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_ID, "")), MyHttp.DELETE, BaseHttpResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .send(new MyHttpCallback<ErrorHttpResponse>() {
                    @Override
                    public void success(ErrorHttpResponse data) {

                        if (data == null) {
                            Utils.logOutUser(getActivity());
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Log.i("GcmDeletion", "Gcm reg failed");
                    }

                    @Override
                    public void onBefore() {

                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }

}
