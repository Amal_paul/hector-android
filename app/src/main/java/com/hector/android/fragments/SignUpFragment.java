package com.hector.android.fragments;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;

import com.hector.android.R;
import com.hector.android.activities.LauncherActivity;
import com.hector.android.adapters.GenderSpinnerAdapter;
import com.hector.android.application.Utils;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.buttonwithprogressbar.CircularProgressButton;
import com.hector.android.pojo.requests.CreateSessionRequest;
import com.hector.android.pojo.requests.DeviceRegRequest;
import com.hector.android.pojo.responses.CreateSessionResponse;
import com.hector.android.pojo.responses.UserDetailResponse;
import com.hector.android.pojo.responses.pateintdevicedetails.DeviceRegResponse;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by arjun on 12/22/15.
 */
public class SignUpFragment extends Fragment {
    private View mRootView;
    private EditText mUserFirstNameEt;
    private AutoCompleteTextView mUserEmailEt;
    private EditText mUserPasswordEt;
    private CircularProgressButton mSubmitBtn;
    private String userEnteredEmail = "";
    private String userEnteredFn = "";
    private String userEnteredLn = "";
    private String userEnteredPw = "";
    private String userEnteredGender = "";
    private Spinner mUserGenderTv;
    private CreateSessionRequest createSessionRequest;
    private String[] genderItems = {"Select gender", "Male", "Female"};
    private EditText mUserLastNameEt;
    private ArrayAdapter<String> emailAdapter;
    private Set<String> availableEmails = new HashSet<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_sign_up, container, false);
        initialiseViews();
        bindEvents();
        return mRootView;
    }

    private void bindEvents() {
      /*
        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (detailsValid()) {
                    mSubmitBtn.setProgress(50);
                    createSessionRequest = new CreateSessionRequest();
                    createSessionRequest.email = userEnteredEmail;
                    createSessionRequest.firstName = userEnteredFn;
                    createSessionRequest.lastName = userEnteredLn;
                    createSessionRequest.password = userEnteredPw;
                    createSessionRequest.gender = userEnteredGender;
                    createSessionRequest.createUser(getActivity(), new MyHttpCallback<UserDetailResponse>() {
                        @Override
                        public void success(UserDetailResponse data) {
                            createSession();
                        }

                        @Override
                        public void failure(String msg) {
                            msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                            Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                            mSubmitBtn.setProgress(0);
                        }

                        @Override
                        public void onBefore() {
                            Utils.showSnackBar(mRootView, getResources().getString(R.string.creating_user), Snackbar.LENGTH_LONG);
                        }

                        @Override
                        public void onFinish() {

                        }
                    });
                }
            }
        });
     */
    }

    private void createSession() {
        createSessionRequest.createSession(getActivity(), userEnteredEmail, userEnteredPw, new MyHttpCallback<CreateSessionResponse>() {
            @Override
            public void success(CreateSessionResponse data) {
                registerDevice();
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                mSubmitBtn.setProgress(0);
            }

            @Override
            public void onBefore() {
                Utils.showSnackBar(mRootView, "Logging in...", Snackbar.LENGTH_LONG);
            }

            @Override
            public void onFinish() {

            }
        });
    }

    private void registerDevice() {
        new DeviceRegRequest().getDevice(getActivity(), new MyHttpCallback<DeviceRegResponse>() {
            @Override
            public void success(DeviceRegResponse data) {
                Intent dashboardIntent = new Intent(getActivity(), LauncherActivity.class);
                dashboardIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(dashboardIntent);
                getActivity().finish();
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                mSubmitBtn.setProgress(0);
            }

            @Override
            public void onBefore() {
                Utils.showSnackBar(mRootView, getResources().getString(R.string.fetching_device_id), Snackbar.LENGTH_LONG);
            }

            @Override
            public void onFinish() {
                /*
                    do not use this
                */
            }
        });
    }

    private boolean detailsValid() {
        boolean isAllValid = true;
        userEnteredEmail = mUserEmailEt.getText().toString().trim();
        userEnteredFn = mUserFirstNameEt.getText().toString().trim();
        userEnteredLn = mUserLastNameEt.getText().toString().trim();
        userEnteredPw = mUserPasswordEt.getText().toString().trim();
        userEnteredGender = mUserGenderTv.getSelectedItem().toString().trim().toLowerCase();
        if(userEnteredFn.isEmpty()){
            mUserFirstNameEt.setError(getResources().getString(R.string.invalid_username));
            isAllValid = false;
        }
        if (userEnteredEmail.isEmpty() || !Utils.isEmailValid(userEnteredEmail)){
            mUserEmailEt.setError(getResources().getString(R.string.email_validation_error));
            isAllValid = false;
        }
        if (mUserGenderTv.getSelectedItemPosition() == 0){
            Utils.showSnackBar(mRootView, getResources().getString(R.string.prompt_gender), Snackbar.LENGTH_LONG);
            isAllValid = false;
        }
        if(userEnteredPw.isEmpty() || userEnteredPw.length() < 8){
            mUserPasswordEt.setError(getResources().getString(R.string.invalid_password));
            isAllValid = false;
        }
        return isAllValid;
    }

    private void initialiseViews() {
        mUserFirstNameEt = (EditText) mRootView.findViewById(R.id.sign_up_userfirstname);
        mUserLastNameEt = (EditText) mRootView.findViewById(R.id.sign_up_userlastname);
        mUserEmailEt = (AutoCompleteTextView) mRootView.findViewById(R.id.sign_up_email);
        mUserPasswordEt = (EditText) mRootView.findViewById(R.id.sign_up_password);
        mUserGenderTv = (Spinner) mRootView.findViewById(R.id.sign_up_gender);
        mSubmitBtn = (CircularProgressButton) mRootView.findViewById(R.id.sign_up_button);
        mSubmitBtn.setIndeterminateProgressMode(true);
        GenderSpinnerAdapter adapter = new GenderSpinnerAdapter(getActivity(), genderItems);
        mUserGenderTv.setAdapter(adapter);

        AccountManager accountManager = (AccountManager) getActivity().getSystemService(Context.ACCOUNT_SERVICE);
        Account[] accounts = accountManager.getAccounts();
        for(Account account : accounts){
            if(Utils.isEmailValid(account.name)) {
                availableEmails.add(account.name);
            }
        }
        emailAdapter = new ArrayAdapter<>(getActivity(), R.layout.single_list_item, new ArrayList(availableEmails));
        mUserEmailEt.setAdapter(emailAdapter);

    }
}
