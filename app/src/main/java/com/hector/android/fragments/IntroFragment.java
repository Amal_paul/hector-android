package com.hector.android.fragments;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.hector.android.R;
import com.hector.android.libs.buttonwithprogressbar.CircularProgressButton;

/**
 * Created by arjun on 12/22/15.
 */
public class IntroFragment extends Fragment {
    private View mRootView;
    private CircularProgressButton mSignInBtn;
    //    private CircularProgressButton mSignUpBtn;
    private TransitionInflater transitionInflater;
    private ImageView appLogoImage;
    private boolean shouldAnimate = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_intro, container, false);
        initialiseView();
        bindEvents();
        setAnimation();
        return mRootView;
    }

    private void setAnimation() {
        if (shouldAnimate) {
            appLogoImage.setVisibility(View.INVISIBLE);
            mSignInBtn.setVisibility(View.GONE);
            shouldAnimate = false;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Animation pulse = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce_anim);
                    Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_up);
                    mSignInBtn.startAnimation(bottomUp);
                    appLogoImage.startAnimation(pulse);
                    appLogoImage.setVisibility(View.VISIBLE);
                    mSignInBtn.setVisibility(View.VISIBLE);
                }
            }, 200);
        }
    }

    private void bindEvents() {
        mSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignInFragment signInFragment = new SignInFragment();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    signInFragment.setSharedElementEnterTransition(transitionInflater.inflateTransition(R.transition.fragment_transition));
                    signInFragment.setEnterTransition(transitionInflater.inflateTransition(android.R.transition.fade));
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.sign_in_container, signInFragment)
                            .addToBackStack("Intro")
                            .addSharedElement(mSignInBtn, getResources().getString(R.string.sign_in))
                            .commit();
                } else {
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.sign_in_container, signInFragment)
                            .addToBackStack("Intro")
                            .commit();
                }
            }
        });
//        mSignUpBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SignUpFragment signUpFragment = new SignUpFragment();
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    signUpFragment.setSharedElementEnterTransition(transitionInflater.inflateTransition(R.transition.fragment_transition));
//                    signUpFragment.setEnterTransition(transitionInflater.inflateTransition(android.R.transition.fade));
//                    getActivity().getSupportFragmentManager()
//                            .beginTransaction()
//                            .replace(R.id.sign_in_container, signUpFragment)
//                            .addToBackStack("Intro")
//                            .addSharedElement(mSignUpBtn, getResources().getString(R.string.sign_up))
//                            .commit();
//                } else {
//                    getActivity().getSupportFragmentManager()
//                            .beginTransaction()
//                            .replace(R.id.sign_in_container, signUpFragment)
//                            .addToBackStack("Intro")
//                            .commit();
//                }
//            }
//        });
    }

    private void initialiseView() {
        appLogoImage = (ImageView) mRootView.findViewById(R.id.app_logo);
        mSignInBtn = (CircularProgressButton) mRootView.findViewById(R.id.intro_sign_in_button);
//        mSignUpBtn = (CircularProgressButton) mRootView.findViewById(R.id.intro_sign_up_button);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            transitionInflater = TransitionInflater.from(getActivity());
            setSharedElementReturnTransition(transitionInflater.inflateTransition(R.transition.fragment_transition));
            setExitTransition(transitionInflater.inflateTransition(android.R.transition.fade));
        }
    }
}
