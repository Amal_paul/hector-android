package com.hector.android.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hector.android.R;
import com.hector.android.adapters.PatientsAdapter;
import com.hector.android.application.DialogClickListener;
import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.DividerItemDecoration;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.requests.PatientsRequest;
import com.hector.android.pojo.responses.patientdetails.Patient;
import com.hector.android.pojo.responses.patientdetails.PatientsResponse;

import java.util.ArrayList;

/**
 * Created by arjun on 12/8/15.
 */
public class SwitchPatientFragment extends Fragment {
    private View mRootView;
    private ProgressBar mListLoadPb;
    private RecyclerView mPatientsListView;
    private ArrayList<Patient> patients = new ArrayList<>();
    private PatientsAdapter mPatientsAdapter;
    private TypeFaceTextView mEmptyDataLabel;
    private Patient me;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_switch_patient, container, false);
        initialiseViews();
        if(patients.size() == 0) {
            addMe();
            fetchAllPatients();
        }
        return mRootView;
    }

    private void addMe() {
        me = new Patient(PatientsAdapter.VIEW_TYPE_DEFAULT);
        me.patientFn = Hector.rootUserName;
        me.patientId = Hector.rootUserId;
        Patient sectionDummyPatient = new Patient(PatientsAdapter.VIEW_TYPE_SECTION);
        sectionDummyPatient.sectionName = "You";
        patients.add(sectionDummyPatient);
        patients.add(me);
    }


    private void initialiseViews() {
        mEmptyDataLabel = (TypeFaceTextView) mRootView.findViewById(R.id.switch_patient_empty_data_ph);
        mListLoadPb = (ProgressBar) mRootView.findViewById(R.id.switch_patient_pb);
        mPatientsListView = (RecyclerView) mRootView.findViewById(R.id.all_patients_list_view);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mPatientsListView.setLayoutManager(mLinearLayoutManager);
        mPatientsListView.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.recyclerview_divider)));
        mPatientsAdapter = new PatientsAdapter(getActivity(), patients, new PatientsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v) {
                showConfirmationDialog(mPatientsListView.getChildAdapterPosition(v));
            }
        });
        mPatientsListView.setAdapter(mPatientsAdapter);
    }

    private void showConfirmationDialog(int position) {
        final Patient patient = patients.get(position);
        SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.CURRENT_USER_ID, patient.patientId);
        SharedPreferencesManager.setStringPreferenceData(SharedPreferencesManager.CURRENT_USER_NAME, patient.getFullName());
        Utils.restartApplication(getActivity());
      /*  Utils.showAlertDialog(getActivity(), getResources().getString(R.string.are_you_sure),
                getResources().getString(R.string.switch_patient_text, patient.patientFn), false, getResources().getString(R.string.confirm_switch_patient),
                getResources().getString(R.string.cancel), new DialogClickListener() {
                    @Override
                    public void onPositiveButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();

                    }

                    @Override
                    public void onNegativeButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onDialogDismissed(AlertDialog alertDialog) {
                    }
                });*/
    }

    private void fetchAllPatients() {
        new PatientsRequest().getMyPatients(getActivity(), new MyHttpCallback<PatientsResponse>() {
            @Override
            public void success(PatientsResponse data) {
                if(data.patients.size() > 0) {
                    Patient patient = new Patient(PatientsAdapter.VIEW_TYPE_SECTION);
                    patient.sectionName = "Your Patients";
                    patients.add(patient);
                    patients.addAll(data.patients);
                } else {
                    Patient patient = new Patient(PatientsAdapter.VIEW_TYPE_SECTION);
                    patient.sectionName = "You don't have any patients";
                    patients.add(patient);
                }
                mPatientsAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
            }

            @Override
            public void onBefore() {
                mListLoadPb.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                mListLoadPb.setVisibility(View.GONE);
            }
        });
    }

}
