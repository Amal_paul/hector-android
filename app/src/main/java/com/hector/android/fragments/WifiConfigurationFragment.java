package com.hector.android.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.InputType;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.hector.android.R;
import com.hector.android.adapters.WifiListAdapter;
import com.hector.android.application.DialogClickListener;
import com.hector.android.application.Utils;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.SmartCommWifiConfigDetails;
import com.hector.android.pojo.requests.DeviceDetailsRequest;
import com.hector.android.pojo.responses.pateintdevicedetails.Device;
import com.hector.android.pojo.responses.pateintdevicedetails.DeviceConfigResponse;

import java.util.ArrayList;

/**
 * Created by arjun on 12/15/15.
 */
public class WifiConfigurationFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private static final int REQUEST_COURSE_LOCATION = 0;
    private View mRootView;
    private WifiListAdapter mWifiResultsAdapter;
    private WifiManager wifiManager;
    private ProgressBar fetchPb;
    private ArrayList<ScanResult> mScanResults = new ArrayList<>();
    private String messageToPublish = "";
    private String smartCommunicatorId = "";
    private String wifi_security_type = "";
    private RecyclerView wifiListView;
    private SwipeRefreshLayout swipeRefresh;
    private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                swipeRefresh.setRefreshing(false);
                mScanResults.addAll(wifiManager.getScanResults());
                mWifiResultsAdapter.notifyDataSetChanged();
            }
        }
    };
    private boolean isPasswordVisible = false;
    private String SMART_COMMUNICATOR_ID = "";
    private TypeFaceTextView enter_wifi_detail;

    private void RequestPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                "android.permission.ACCESS_COARSE_LOCATION")
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(
                    new String[]{"android.permission.ACCESS_COARSE_LOCATION"},
                    REQUEST_COURSE_LOCATION);

       /*     // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    "android.permission.ACCESS_COARSE_LOCATION")) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{"android.permission.ACCESS_COARSE_LOCATION"},
                        REQUEST_COURSE_LOCATION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }*/
        } else {
            registerReceiver();
            fetchWifiList();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_COURSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    registerReceiver();
                    fetchWifiList();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_wifi_config, container, false);
        wifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        initialiseViews();
        bindEvents();
        RequestPermissions();
        SMART_COMMUNICATOR_ID = getArguments().getString(Device.DEVICE_TYPE_SMART_CONNECTOR);
        enter_wifi_detail = (TypeFaceTextView) mRootView.findViewById(R.id.enter_wifi_detail);
        enter_wifi_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOtherWifiDetailDialog();
            }
        });

        return mRootView;
    }


    private void registerReceiver() {
        getActivity().registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    private void fetchWifiList() {
        swipeRefresh.setRefreshing(true);
        wifiManager.startScan();
    }

    private void bindEvents() {
        mWifiResultsAdapter.setOnItemClickListener(new WifiListAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int position) {
                showWifiDetailDialog(position);
            }
        });
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mScanResults.clear();
                fetchWifiList();
            }
        });
    }

    private void showOtherWifiDetailDialog() {
        isPasswordVisible = false;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_device_config, null);
        builder.setView(view);
        final TypeFaceTextView wifiNameView = (TypeFaceTextView) view.findViewById(R.id.prompt_dialog_title_text);
        final TypeFaceTextView messageView = (TypeFaceTextView) view.findViewById(R.id.prompt_dialog_message_text);
        final TypeFaceTextView positiveBtn = (TypeFaceTextView) view.findViewById(R.id.prompt_dialog_positive_btn);
        final TypeFaceTextView negativeBtn = (TypeFaceTextView) view.findViewById(R.id.prompt_dialog_negative_btn);
        final EditText ssidBox = (EditText) view.findViewById(R.id.prompt_dialog_ssid_text_box);
        wifiNameView.setVisibility(View.GONE);
        String PasswordPrompMsg;
        messageView.setText("Enter wifi name and password.");
        final EditText passwordText = (EditText) view.findViewById(R.id.prompt_dialog_text_box);
        final Spinner wifiSecurityType = (Spinner) view.findViewById(R.id.wifiSecurityTypeSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.wifi_security_type, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        wifiSecurityType.setAdapter(adapter);
        wifiSecurityType.setOnItemSelectedListener(this);
        final ImageView passwordVisibilityCb = (ImageView) view.findViewById(R.id.prompt_dialog_pw_visibility_switcher);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.show();
        negativeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String enteredPw = passwordText.getText().toString().trim();
                if (!ssidBox.getText().toString().isEmpty()){
                if (!enteredPw.isEmpty()) {
                    if (!wifi_security_type.isEmpty()) {
                        alertDialog.dismiss();
                        configureDevice(ssidBox.getText().toString(), enteredPw, wifi_security_type);
                    } else {
                        // Utils.showSnackBar(mRootView,"Select wifi security type",Snackbar.LENGTH_LONG);
                        Toast.makeText(getActivity(), "Choose wifi security type", Toast.LENGTH_LONG).show();
                    }
                } else {
                    passwordText.setError(getResources().getString(R.string.password_validation_error));
                }
            }else {
                    Toast.makeText(getActivity(), "Enter wifi name", Toast.LENGTH_LONG).show();
                }
            }
        });
        passwordVisibilityCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPasswordVisible) {
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordVisibilityCb.setImageResource(R.drawable.icon_pw_visible);
                    isPasswordVisible = false;
                } else {
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT);
                    passwordVisibilityCb.setImageResource(R.drawable.icon_pw_invisible);
                    isPasswordVisible = true;
                }
            }
        });
    }


    private void showWifiDetailDialog(int position) {
        isPasswordVisible = false;
        final ScanResult scanResult = mScanResults.get(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_device_config, null);
        builder.setView(view);
        final TypeFaceTextView wifiNameView = (TypeFaceTextView) view.findViewById(R.id.prompt_dialog_title_text);
        final TypeFaceTextView messageView = (TypeFaceTextView) view.findViewById(R.id.prompt_dialog_message_text);
        final TypeFaceTextView positiveBtn = (TypeFaceTextView) view.findViewById(R.id.prompt_dialog_positive_btn);
        final TypeFaceTextView negativeBtn = (TypeFaceTextView) view.findViewById(R.id.prompt_dialog_negative_btn);
        final LinearLayout ssidBox = (LinearLayout) view.findViewById(R.id.ssid_box);
        wifiNameView.setVisibility(View.GONE);
        ssidBox.setVisibility(View.GONE);
        String PasswordPrompMsg;
        if (scanResult.SSID.isEmpty()) {
            PasswordPrompMsg = getResources().getString(R.string.password_prompt) + "<b>" + " unknown" + "</b>";
        } else
            PasswordPrompMsg = getResources().getString(R.string.password_prompt) + " " + "<b>" + scanResult.SSID + "</b>";
        messageView.setText(Html.fromHtml(PasswordPrompMsg));
        // wifiNameView.setText(scanResult.SSID.isEmpty() ? "Unknown" : scanResult.SSID);
        final EditText passwordText = (EditText) view.findViewById(R.id.prompt_dialog_text_box);
        final Spinner wifiSecurityType = (Spinner) view.findViewById(R.id.wifiSecurityTypeSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.wifi_security_type, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        wifiSecurityType.setAdapter(adapter);
        wifiSecurityType.setOnItemSelectedListener(this);
        final ImageView passwordVisibilityCb = (ImageView) view.findViewById(R.id.prompt_dialog_pw_visibility_switcher);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.show();
        negativeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        positiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String enteredPw = passwordText.getText().toString().trim();
                if (!enteredPw.isEmpty()) {
                    if (!wifi_security_type.isEmpty()) {
                        alertDialog.dismiss();
                        configureDevice(scanResult.SSID, enteredPw, wifi_security_type);
                    } else {
                        // Utils.showSnackBar(mRootView,"Select wifi security type",Snackbar.LENGTH_LONG);
                        Toast.makeText(getActivity(), "Choose wifi security type", Toast.LENGTH_LONG).show();
                    }
                } else {
                    passwordText.setError(getResources().getString(R.string.password_validation_error));
                }
            }
        });
        passwordVisibilityCb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPasswordVisible) {
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordVisibilityCb.setImageResource(R.drawable.icon_pw_visible);
                    isPasswordVisible = false;
                } else {
                    passwordText.setInputType(InputType.TYPE_CLASS_TEXT);
                    passwordVisibilityCb.setImageResource(R.drawable.icon_pw_invisible);
                    isPasswordVisible = true;
                }
            }
        });
    }

    private void configureDevice(String ssid, String enteredPw, String wifiSecurityType) {
        DeviceDetailsRequest configRequest = new DeviceDetailsRequest();
        configRequest.smartCommWifiConfigDetails = new SmartCommWifiConfigDetails();
        configRequest.smartCommWifiConfigDetails.wifiName = ssid;
        configRequest.smartCommWifiConfigDetails.wifiPassword = enteredPw;
        configRequest.smartCommWifiConfigDetails.securityType = wifiSecurityType;
        configRequest.configureDeviceWithWifi(getActivity(), SMART_COMMUNICATOR_ID, new MyHttpCallback<DeviceConfigResponse>() {
            @Override
            public void success(DeviceConfigResponse data) {
                //   showDialog(getResources().getString(R.string.device_configured), getResources().getString(R.string.device_configure_detail));
                getActivity().onBackPressed();
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                showDialog(getResources().getString(R.string.device_not_found), getResources().getString(R.string.device_not_found_detail));

            }

            @Override
            public void onBefore() {
                Utils.showLoadingDialog(getActivity(), getResources().getString(R.string.please_wait), false);
            }

            @Override
            public void onFinish() {
                Utils.dismissLoadingDialog();
            }
        });
    }

    private void showDialog(String title, String message) {
        Utils.showAlertDialog(getActivity(), title,
                message, true, getResources().getString(R.string.ok),
                "", new DialogClickListener() {
                    @Override
                    public void onPositiveButtonClicked(AlertDialog alertDialog) {
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClicked(AlertDialog alertDialog) {
                    }

                    @Override
                    public void onDialogDismissed(AlertDialog alertDialog) {
                    }
                });
    }

    private void initialiseViews() {
        swipeRefresh = (SwipeRefreshLayout) mRootView.findViewById(R.id.wifi_swipe_refresh);
        wifiListView = (RecyclerView) mRootView.findViewById(R.id.wifi_connections);
        fetchPb = (ProgressBar) mRootView.findViewById(R.id.details_fetch_progress);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        wifiListView.setLayoutManager(linearLayoutManager);
        mWifiResultsAdapter = new WifiListAdapter(getActivity(), mScanResults);
        wifiListView.setAdapter(mWifiResultsAdapter);
        TypedValue typed_value = new TypedValue();
        getActivity().getTheme().resolveAttribute(android.support.v7.appcompat.R.attr.actionBarSize, typed_value, true);
        swipeRefresh.setProgressViewOffset(false, 0, getResources().getDimensionPixelSize(typed_value.resourceId));
    }


    @Override
    public void onPause() {
        super.onPause();
        try {
            getActivity().unregisterReceiver(mWifiScanReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            wifi_security_type = "";
        } else {
            wifi_security_type = (String) parent.getItemAtPosition(position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
