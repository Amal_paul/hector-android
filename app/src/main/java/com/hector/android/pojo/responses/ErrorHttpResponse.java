package com.hector.android.pojo.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amal on 13/01/16.
 */
public class ErrorHttpResponse {
    @Expose
    @SerializedName("code")
    public String code;
    @Expose
    @SerializedName("message")
    public String message;

}
