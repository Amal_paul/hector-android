package com.hector.android.pojo.responses.karmadetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by amal on 18/03/16.
 */
public class medicationAdherence {

    @Expose
    @SerializedName("medicationComplianceRecordCount")
    public int medicationComplianceRecordCount;

    @Expose
    @SerializedName("medicationComplianceIndex")
    public double medicationComplianceIndex;

    @Expose
    @SerializedName("medicationAdherenceIndex")
    public double medicationAdherenceIndex;

}
