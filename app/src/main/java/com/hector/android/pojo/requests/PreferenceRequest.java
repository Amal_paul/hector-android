package com.hector.android.pojo.requests;

import android.content.Context;

import com.hector.android.application.Hector;
import com.hector.android.application.SharedPreferencesManager;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.PreferencesResponse;

import java.util.HashMap;

/**
 * Created by arjun on 12/9/15.
 */
public class PreferenceRequest {
    public Preferences settings;

    public void updateNotificationSettings(Context context, final Preferences preferences, final MyHttpCallback<PreferencesResponse> myHttpCallback){
        this.settings = preferences;
        HashMap<String,String> map = new HashMap<>();
        map.put("push", String.valueOf(preferences.push));
        map.put("email",String.valueOf(preferences.email));
        map.put("sms",String.valueOf(preferences.sms));

                MyHttp.getInstance(context, Endpoints.getPreference(SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_ID, "")), MyHttp.PUT, PreferencesResponse.class)
                        .addJson(map)
                        .setDefaultHeaders()
                        .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                        .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                        .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                        .send(new MyHttpCallback<PreferencesResponse>() {
                            @Override
                            public void success(PreferencesResponse data) {
                                if (data == null) {
                                    SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.SMS_NOTIFICATION_PREFERENCE, preferences.sms);
                                    SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.EMAIL_NOTIFICATION_PREFERENCE, preferences.email);
                                    SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.PUSH_NOTIFICATION_PREFERENCE, preferences.push);
                                    myHttpCallback.success(data);
                                } else {
                                    myHttpCallback.failure(data.message);
                                }
                            }

                            @Override
                            public void failure(String msg) {
                                myHttpCallback.failure(msg);
                            }

                            @Override
                            public void onBefore() {
                                myHttpCallback.onBefore();
                            }

                            @Override
                            public void onFinish() {
                                myHttpCallback.onFinish();
                            }
                        });
    }

    public void getPreferenceSettings(Context context, final MyHttpCallback<PreferencesResponse> myHttpCallback){

        MyHttp.getInstance(context, Endpoints.getPreference(SharedPreferencesManager.getStringPreference(SharedPreferencesManager.USER_ID,"")), MyHttp.GET, PreferencesResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<PreferencesResponse>() {
                    @Override
                    public void success(PreferencesResponse data) {
                        if(data.code == null){
                           // SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.SMS_NOTIFICATION_PREFERENCE, data.sms);
                            SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.EMAIL_NOTIFICATION_PREFERENCE, data._email);
                            SharedPreferencesManager.setBooleanPreference(SharedPreferencesManager.PUSH_NOTIFICATION_PREFERENCE, data._push);
                            myHttpCallback.success(data);
                        } else {
                            myHttpCallback.failure(data.message);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        myHttpCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        myHttpCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        myHttpCallback.onFinish();
                    }
                });
    }

}
