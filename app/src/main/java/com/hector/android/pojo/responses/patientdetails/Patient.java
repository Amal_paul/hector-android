package com.hector.android.pojo.responses.patientdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by arjun on 12/8/15.
 */
public class Patient {
    @Expose
    @SerializedName("patientId")
    public String patientId;

    @Expose
    public ArrayList<CareGiver> careGivers;

    public String getFullName() {
        String lastName = this.patientLn != null ? this.patientLn : "";
        return this.patientFn + " " + lastName;
    }

    public class CareGiver {
        @Expose
        @SerializedName("id")
        public String careGiverId;
    }

    @Expose
    @SerializedName("firstName")
    public String patientFn;

    @Expose
    @SerializedName("lastName")
    public String patientLn;

    @Expose
    @SerializedName("email")
    public String patientEmail;

    @Expose
    @SerializedName("phoneNumber")
    public String patientPn;

    @Expose
    @SerializedName("gender")
    public String patientGender;

    /*@Expose
    @SerializedName("address")
    public String patientAddress;*/

    @Expose
    public Meta meta;

    public class Meta{
        @Expose
        @SerializedName("pharmacistId")
        public String patientPharmacistId;
    }

    @Expose
    @SerializedName("password")
    public String patientPassword;

    @Expose
    public Role role;

    public class Role{
        @Expose
        @SerializedName("patient")
        public boolean isPatient;
    }

    @Expose
    @SerializedName("settings")
    public Preference preference;
    
    public class Preference{
        @Expose
        @SerializedName("push")
        public boolean pushNotificationPrefs;

        @Expose
        @SerializedName("email")
        public boolean emailNotificationPrefs;

        @Expose
        @SerializedName("sms")
        public boolean smsNotificationPrefs;
    }

    @Expose
    public String datatype;

    //used with adapter to show sections
    public String sectionName;
    //used with adapter to know the view type
    public int viewType;
    public Patient(int viewType) {
        this.viewType = viewType;
    }
}
