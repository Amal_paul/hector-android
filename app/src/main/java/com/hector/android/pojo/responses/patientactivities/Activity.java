package com.hector.android.pojo.responses.patientactivities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by arjun on 11/23/15.
 */
public class Activity {
    public Activity() {
    }

    public Activity(int viewType) {
        this.viewType = viewType;
    }

    @Expose
    @SerializedName("_id")
    public String activityId;

    @Expose
    public Actor actor;

    public Activity(int viewType, String timeStamp) {
        this.viewType = viewType;
        this.sectionDetails = timeStamp;
    }

    public class Actor{

        @Expose
        @SerializedName("id")
        public String actorId;
        @Expose
        @SerializedName("identifier")
        public String actorName;

        @Expose
        @SerializedName("type")
        public String actorType;

    }
    @Expose
    @SerializedName("action")
    public String activityAction;

    @Expose
    @SerializedName("level")
    public String activityLevel;

    @Expose
    @SerializedName("metaData")
    public MedicationMeta medicationMeta;

    public class MedicationMeta{
        @Expose
        @SerializedName("details")
        public ArrayList<DrugDetail> drugDetails;

        @Expose
        public String medicationTakenAt;

        @Expose
        public String medicationAt;
    }


    public class DrugDetail {
        @Expose
        public String drug;
        @Expose
        public String quantity;

        @Expose
        public String description;

    }
    @Expose
    @SerializedName("datatype")
    public String dataType;

    @Expose
    public String createdAt;

    @Expose
    @SerializedName("dateTime")
    public String dateTime;

    @Expose
    @SerializedName("message")
    public String message;

    public String sectionDetails;

    public int viewType;
}
