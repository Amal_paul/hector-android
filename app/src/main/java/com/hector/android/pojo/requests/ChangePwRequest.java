package com.hector.android.pojo.requests;

import android.content.Context;

import com.hector.android.application.Hector;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.pojo.responses.ChangePwResponse;

/**
 * Created by arjun on 12/28/15.
 */
public class ChangePwRequest {
    public String oldpassword;
    public String newpassword;

    public ChangePwRequest(String oldpassword, String newpassword) {
        this.oldpassword = oldpassword;
        this.newpassword = newpassword;
    }

    public void changePassword(Context context, final MyHttpCallback<ChangePwResponse> myHttpCallback){
        MyHttp.getInstance(context, Endpoints.changePassword, MyHttp.POST, ChangePwResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .addJson(this)
                .send(new MyHttpCallback<ChangePwResponse>() {
                    @Override
                    public void success(ChangePwResponse data) {
                        if(data.code == MyHttp.REQUEST_OK){
                            myHttpCallback.success(data);
                        } else {
                            myHttpCallback.failure(data.message);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        myHttpCallback.failure(msg);
                    }

                    @Override
                    public void onBefore() {
                        myHttpCallback.onBefore();
                    }

                    @Override
                    public void onFinish() {
                        myHttpCallback.onFinish();
                    }
                });
    }
}
