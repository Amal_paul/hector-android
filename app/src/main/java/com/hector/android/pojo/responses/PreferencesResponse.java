package com.hector.android.pojo.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by arjun on 12/9/15.
 */
public class PreferencesResponse extends ErrorHttpResponse {
    @Expose
    public GenericResponse body;

    @Expose
    @SerializedName("push")
    public boolean _push;

    @Expose
    @SerializedName("email")
    public boolean _email;

}
