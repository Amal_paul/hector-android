package com.hector.android.pojo.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by arjun on 11/26/15.
 */
public class UserDetailResponse extends ErrorHttpResponse {
    // public Patient body;

    @Expose
    @SerializedName("firstName")
    public String firstName;

    @Expose
    @SerializedName("lastName")
    public String lastName;

    @Expose
    @SerializedName("email")
    public String email;

    @Expose
    @SerializedName("gender")
    public String gender;

    @Expose
    @SerializedName("patientId")
    public String patientId;

    public String getFullName() {
        String lastName = this.lastName != null ? this.lastName : "";
        return this.firstName + " " + lastName;
    }

}
