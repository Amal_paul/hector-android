package com.hector.android.pojo;

/**
 * Created by arjun on 12/15/15.
 */
public class SmartCommWifiConfigDetails {
    public String wifiName;
    public String wifiPassword;
    public String securityType;
    public String communicatorId;
    public String patientId;
}
