package com.hector.android.pojo.responses;

import com.google.gson.annotations.Expose;

/**
 * Created by arjun on 11/24/15.
 */
public class BaseHttpResponse {
    @Expose
    public Integer code;
    @Expose
    public String message;

    public Detail detail;

    public class Detail {
        @Expose
        public String message;
        @Expose
        public int code;
        @Expose
        public String name;
        @Expose
        public boolean visible;
    }
}

