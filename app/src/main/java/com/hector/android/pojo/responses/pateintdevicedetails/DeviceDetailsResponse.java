package com.hector.android.pojo.responses.pateintdevicedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.BaseHttpResponse;
import com.hector.android.pojo.responses.ErrorHttpResponse;

import java.util.ArrayList;

/**
 * Created by arjun on 12/12/15.
 */
public class DeviceDetailsResponse extends ErrorHttpResponse {

    @Expose
    @SerializedName("devices")
    public ArrayList <Device> device;
}
