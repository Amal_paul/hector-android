package com.hector.android.pojo.responses.patientdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.BaseHttpResponse;
import com.hector.android.pojo.responses.ErrorHttpResponse;

import java.util.ArrayList;

/**
 * Created by arjun on 12/8/15.
 */
public class PatientsResponse extends ErrorHttpResponse {
    @Expose
    @SerializedName("patients")
    public ArrayList<Patient> patients;
}
