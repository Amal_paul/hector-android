package com.hector.android.pojo.responses.patientactivities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by amal on 22/01/16.
 */
public class dayActivities {
    @Expose
    @SerializedName("activities")
    public ArrayList<Activity> activities;

}
