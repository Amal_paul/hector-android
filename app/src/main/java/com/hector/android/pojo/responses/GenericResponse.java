package com.hector.android.pojo.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by arjun on 12/23/15.
 */
public class GenericResponse {
    @Expose
    @SerializedName("ok")
    public boolean isRequestSuccess;
}
