package com.hector.android.pojo.responses.pateintdevicedetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by arjun on 12/12/15.
 */
public class Device {
    public static String DEVICE_TYPE_FSBL = "fsbl";
    public static String DEVICE_TYPE_SMART_CONNECTOR = "smart-connector";

    @Expose
    @SerializedName("deviceId")
    public String deviceId;

    @Expose
    public String vendorId;

    @Expose
    public String patientId;

    @Expose
    @SerializedName("type")
    public String deviceType;

    @Expose
    public String modifiedAt;

    @Expose
    @SerializedName("active")
    public boolean isActive;

    @Expose
    @SerializedName("wifi")
    public boolean isUsingWifi;

    @Expose
    @SerializedName("assigned")
    public boolean isAssigned;
}
