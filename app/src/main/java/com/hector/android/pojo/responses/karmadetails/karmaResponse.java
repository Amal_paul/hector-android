package com.hector.android.pojo.responses.karmadetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hector.android.pojo.responses.ErrorHttpResponse;

import java.util.ArrayList;

/**
 * Created by amal on 18/03/16.
 */
public class karmaResponse extends ErrorHttpResponse {

    @Expose
    @SerializedName("totalKarma")
    public double totalKarma;

    @Expose
    @SerializedName("healthIndexData")
    public ArrayList<healthIndexData> healthIndexDatas;
}
