package com.hector.android.http;

/**
 * Created by Arjun on 8/7/15.
 */
public interface MyHttpCallback<T> {
    void success(T data);

    void failure(String msg);

    void onBefore();

    void onFinish();
}
