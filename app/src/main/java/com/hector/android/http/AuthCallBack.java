package com.hector.android.http;

/**
 * Created by Arjun on 8/7/15.
 */
public interface AuthCallBack {
    void authSuccess();

    void authFailure();
}
