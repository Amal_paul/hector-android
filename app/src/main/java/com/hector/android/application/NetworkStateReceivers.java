package com.hector.android.application;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.hector.android.activities.DialogHolderActivity;

/**
 * Created by Arjun on 8/11/15.
 */
public class NetworkStateReceivers extends BroadcastReceiver {

    private static final String TAG = "NetworkStateReceivers";
    public static final String NETWORK_CONNECTION_STATE_CHANGED = "com.hector.android.NETWORK_CONNECTION_STATE_CHANGED";
    private static String lastActiveNetworkName = null;

    public static void setLastActiveNetworkName(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo network = cm.getActiveNetworkInfo();
        if (network != null) {
            lastActiveNetworkName = network.getTypeName();
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Network state received");
        resetNetworkState(context);
    }

    private static void sendBroadcast(Context context) {
        Intent connectivityStateChangeIntent = new Intent(NETWORK_CONNECTION_STATE_CHANGED);
        LocalBroadcastManager.getInstance(context).sendBroadcast(connectivityStateChangeIntent);
    }

    public static void resetNetworkState(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            Log.i(TAG, "Connectivity Manager is null!");
            return;
        }
        NetworkInfo network = cm.getActiveNetworkInfo();
        if (network != null && network.isConnected()) {
            if(Hector.isConnectedToInternet){return;}
            Hector.isConnectedToInternet = true;
            sendBroadcast(context);
            Log.i(TAG, "Network Connected");
        } else {
            if(Hector.isConnectedToInternet) {
                Hector.isConnectedToInternet = false;
                showNoConnectivityDialog(context);
//                sendBroadcast(context);
                Log.i(TAG, "Network Disconnected");
            }
        }
    }

    public static void showNoConnectivityDialog(Context context){
        Intent dialogIntent = new Intent(context, DialogHolderActivity.class);
        dialogIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(dialogIntent);
    }

}
