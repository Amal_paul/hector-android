package com.hector.android.application;

import android.graphics.Typeface;

/**
 * Created by arjun on 11/20/15.
 */
public class Fonts {
//    public static Typeface BLACK;
//    public static Typeface BLACK_ITALIC;
//    public static Typeface BOLD_ITALIC;
    public static Typeface BOLD;
//    public static Typeface HAIRLINE;
//    public static Typeface HAIRLINE_ITALIC;
//    public static Typeface LIGHT;
//    public static Typeface LIGHT_ITALIC;
//    public static Typeface ITALIC;
    public static Typeface REGULAR;
    public static Typeface MEDIUM;


    public static void initialiseFonts() {
//        BLACK = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Lato-Black.ttf");
//        BLACK_ITALIC = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Lato-BlackItalic.ttf");
        BOLD = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Font-Heavy.ttf");
//        BLACK_ITALIC = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Lato-BoldItalic.ttf");
//        HAIRLINE = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Lato-Hairline.ttf");
//        HAIRLINE_ITALIC = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Lato-HairlineItalic.ttf");
//        ITALIC = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Lato-Italic.ttf");
//        LIGHT = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Lato-Light.ttf");
//        LIGHT_ITALIC = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Lato-LightItalic.ttf");
        REGULAR = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Font-Normal.ttf");
        MEDIUM = Typeface.createFromAsset(Hector.applicationContext.getAssets(), "fonts/Font-Medium.ttf");
    }
}
