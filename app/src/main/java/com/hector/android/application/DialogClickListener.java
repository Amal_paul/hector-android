package com.hector.android.application;

import android.app.AlertDialog;

/**
 * Created by arjun on 12/24/15.
 */
public interface DialogClickListener {
    void onPositiveButtonClicked(AlertDialog alertDialog);
    void onNegativeButtonClicked(AlertDialog alertDialog);
    void onDialogDismissed(AlertDialog alertDialog);
}
