package com.hector.android.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hector.android.R;
import com.hector.android.application.Hector;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.responses.patientdetails.Patient;

import java.util.ArrayList;

/**
 * Created by arjun on 12/8/15.
 */
public class PatientsAdapter extends RecyclerView.Adapter<PatientsAdapter.ViewHolder> {
    public static final int VIEW_TYPE_DEFAULT = 0;
    public static final int VIEW_TYPE_SECTION = 1;
    private static OnItemClickListener onItemClickListener = null;
    private final LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<Patient> mPatients;

    public PatientsAdapter(Context context, ArrayList<Patient> patients, OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.mPatients = patients;
        PatientsAdapter.onItemClickListener = onItemClickListener;
        this.mLayoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ViewHolder.createViewHolder(parent, viewType, mLayoutInflater);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Patient currentPatient = mPatients.get(position);
        holder.bindData(mContext, currentPatient, position);
    }

    @Override
    public int getItemCount() {
        return mPatients.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mPatients.get(position).viewType;
    }

    public interface OnItemClickListener {
        void onItemClick(View v);
    }

    static abstract class ViewHolder extends RecyclerView.ViewHolder {


        public ViewHolder(View itemView) {
            super(itemView);
        }

        public static ViewHolder createViewHolder(ViewGroup parent, int viewType, LayoutInflater mLayoutInflater) {
            View view;
            switch (viewType) {
                default:
                case VIEW_TYPE_DEFAULT:
                    view = mLayoutInflater.inflate(R.layout.patient_list_item, parent, false);
                    return new ListItemViewHolder(view);

                case VIEW_TYPE_SECTION:
                    view = mLayoutInflater.inflate(R.layout.section_item_view, parent, false);
                    return new SectionItemViewHolder(view);
            }
        }

        abstract void bindData(Context context, Patient patient, int position);
    }

    static class SectionItemViewHolder extends ViewHolder {
        TypeFaceTextView sectionName;

        public SectionItemViewHolder(View itemView) {
            super(itemView);
            sectionName = (TypeFaceTextView) itemView.findViewById(R.id.section_name_view);
        }

        @Override
        void bindData(Context context, Patient currentPatient, int position) {
            sectionName.setText(currentPatient.sectionName);
        }
    }

    static class ListItemViewHolder extends ViewHolder {
        public TypeFaceTextView patientName;
        public LinearLayout rootView;
        public ImageView checkMark;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            patientName = (TypeFaceTextView) itemView.findViewById(R.id.patient_name_tv);
            rootView = (LinearLayout) itemView.findViewById(R.id.patient_list_item_rootView);
            checkMark = (ImageView) itemView.findViewById(R.id.current_patient_check);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(v);
                }
            });
        }

        @Override
        void bindData(Context context, Patient currentPatient, int position) {
            patientName.setText(currentPatient.getFullName());

            if (currentPatient.patientId.equals(Hector.currentPatientId)) {
                rootView.setEnabled(false);
                checkMark.setVisibility(View.VISIBLE);
            } else {
                rootView.setEnabled(true);
                checkMark.setVisibility(View.GONE);
            }
        }
    }
}
