package com.hector.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hector.android.R;

/**
 * Created by arjun on 12/22/15.
 */
public class GenderSpinnerAdapter extends ArrayAdapter<String> {

    private final Context context;
    private String[] genderItems;

    public GenderSpinnerAdapter(Context context, String[] genderItems) {
        super(context, -1, genderItems);
        this.context = context;
        this.genderItems = genderItems;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String item = genderItems[position];
        SelectedItemViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.gender_item_selected, null);
            viewHolder = new SelectedItemViewHolder();
            viewHolder.selectedItemView = (TextView) convertView.findViewById(R.id.gender_selected_item_view);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SelectedItemViewHolder) convertView.getTag();
        }
        viewHolder.selectedItemView.setText(item);
        if (position == 0) {
            viewHolder.selectedItemView.setTextColor(context.getResources().getColor(R.color.grey13));
        } else {
            viewHolder.selectedItemView.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        }
        return convertView;
    }

    private View getCustomView(int position, View convertView, ViewGroup parent) {
        String item = genderItems[position];
        ViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.gender_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.itemView = (TextView) convertView.findViewById(R.id.gender_list_item_view);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.itemView.setText(item);
        return convertView;

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return genderItems.length;
    }

    static class ViewHolder {
        TextView itemView;
    }

    static class SelectedItemViewHolder {
        TextView selectedItemView;
    }
}
