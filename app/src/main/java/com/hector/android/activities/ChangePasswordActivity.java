package com.hector.android.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.hector.android.R;
import com.hector.android.fragments.ChangePasswordFragment;

public class ChangePasswordActivity extends AppCompatActivity {

    private View mRootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        mRootView = findViewById(R.id.change_pw_root_view);
        setUpActionbar();
        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.change_pw_container, new ChangePasswordFragment())
                    .commit();
        }
    }

    private void setUpActionbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.change_pw_toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(R.drawable.back_icon_white);
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed(){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mRootView.getWindowToken(), 0);
        super.onBackPressed();
    }
}
