package com.hector.android.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hector.android.R;
import com.hector.android.application.Hector;
import com.hector.android.application.NetworkStateReceivers;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.requests.DeviceDetailsRequest;
import com.hector.android.pojo.responses.pateintdevicedetails.Device;
import com.hector.android.pojo.responses.pateintdevicedetails.DeviceDetailsResponse;
import com.hector.android.pojo.responses.pateintdevicedetails.PatientBlisterResponse;

public class DevicesActivity extends AppCompatActivity {

    //    private RecyclerView mDevicesListView;
//    private ArrayList<Device> devicesList = new ArrayList<>();
//    private DevicesAdapter mDevicesAdapter;
    private LinearLayout mRootView;
    private ProgressBar mDevicePb;
    private BroadcastReceiver networkStateReceiver;
    private TypeFaceTextView mDeviceStatusView;
    private TypeFaceTextView mDeviceConnMode;
    private TypeFaceTextView mFsblStatusView;
    private TypeFaceTextView mFsblConnMode;
    private TypeFaceTextView mFsblId;
    private TypeFaceTextView mSmartConnectorId;
    private ProgressBar mFsblPb;
    private LinearLayout mFsblContainer;
    private LinearLayout mDeviceContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setUpActionBar();
        initialiseViews();
        registerNetWorkStateReceiver();
        if (!Hector.isConnectedToInternet) {
            NetworkStateReceivers.showNoConnectivityDialog(DevicesActivity.this);
            return;
        }
        fetchDevicesDetails();
    }

    private void fetchFsblDetails(String deviceId) {
        MyHttp.getInstance(DevicesActivity.this, Endpoints.getAssociatedFsbl(deviceId), MyHttp.GET, PatientBlisterResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<PatientBlisterResponse>() {
                    @Override
                    public void success(PatientBlisterResponse data) {
                        if (data.code == MyHttp.REQUEST_OK) {
                            mFsblStatusView.setText(data.body.isConnected ? getResources().getString(R.string.connected) : getResources().getString(R.string.disconnected));
                            mFsblStatusView.setTextColor(data.body.isConnected ? getResources().getColor(R.color.green) : getResources().getColor(R.color.cpb_red));
                            mFsblConnMode.setText(data.body.isConnected ? "Bluetooth" : "NA");
                        } else if (data.code == 1055) {
                            mFsblStatusView.setText(data.message);
                            mFsblStatusView.setTextColor(getResources().getColor(R.color.cpb_red));
                            mFsblConnMode.setText("NA");
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Toast.makeText(DevicesActivity.this, getResources().getString(R.string.http_error), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onBefore() {
                        mFsblPb.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFinish() {
                        mFsblPb.setVisibility(View.GONE);
                        mFsblContainer.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void registerNetWorkStateReceiver() {
        networkStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Hector.isConnectedToInternet) {
                }
            }
        };
        LocalBroadcastManager.getInstance(DevicesActivity.this).registerReceiver(networkStateReceiver, new IntentFilter(NetworkStateReceivers.NETWORK_CONNECTION_STATE_CHANGED));
    }

    private void fetchDevicesDetails() {
        new DeviceDetailsRequest().getDevicesOfPatients(DevicesActivity.this, new MyHttpCallback<DeviceDetailsResponse>() {
            @Override
            public void success(DeviceDetailsResponse data) {
                boolean fsbl_flag = false, smart_connector_flag = false;

                if (data.code == null && data.device != null) {
                    //  fetchFsblDetails(data.device.deviceId);
                    if (data.device.isEmpty()) {
                        String noDeviceLabel = getResources().getString(R.string.device_not_found);
                        mDeviceStatusView.setText(noDeviceLabel);
                        mDeviceConnMode.setText("NA");
                        mSmartConnectorId.setText("--");
                        mFsblStatusView.setText(noDeviceLabel);
                        mFsblConnMode.setText("NA");
                        mFsblId.setText("--");
                        mFsblPb.setVisibility(View.GONE);
                        mFsblContainer.setVisibility(View.VISIBLE);
                        return;
                    }

                    for (int j = 0; j < data.device.size(); j++) {
                        if (data.device.get(j).deviceType.equals(Device.DEVICE_TYPE_SMART_CONNECTOR)) {
                            if (data.device.get(j).isAssigned) {
                                smart_connector_flag = true;
                                if (data.device.get(j).isActive) {
                                    mDeviceStatusView.setText(getResources().getString(R.string.connected));
                                    mDeviceStatusView.setTextColor(getResources().getColor(R.color.green));
                                    mSmartConnectorId.setText(data.device.get(j).deviceId);
                                    //  mDeviceConnMode.setText(data.device.isUsingWifi ? "WIFI" : "GSM");
                                } else {
                                    mDeviceStatusView.setText(getResources().getString(R.string.disconnected));
                                    mSmartConnectorId.setText(data.device.get(j).deviceId);
                                    mDeviceStatusView.setTextColor(getResources().getColor(R.color.cpb_red));
                                    mDeviceConnMode.setText("NA");
                                }
                            }
                        }

                        if (data.device.get(j).deviceType.equals(Device.DEVICE_TYPE_FSBL)) {
                            if (data.device.get(j).isAssigned) {
                                fsbl_flag = true;
                                mFsblStatusView.setText(data.device.get(j).isActive ? getResources().getString(R.string.connected) : getResources().getString(R.string.disconnected));
                                mFsblId.setText(data.device.get(j).deviceId);
                                mFsblStatusView.setTextColor(data.device.get(j).isActive ? getResources().getColor(R.color.green) : getResources().getColor(R.color.cpb_red));
                                mFsblConnMode.setText(data.device.get(j).isActive ? "Bluetooth" : "NA");
                            }
                        }
                    }
                    //check for empty fsbl and smart-conector
                    if (fsbl_flag == false) {
                        String noDeviceLabel = getResources().getString(R.string.device_not_found);
                        mFsblStatusView.setText(noDeviceLabel);
                        mFsblConnMode.setText("NA");
                        mFsblId.setText("--");
                        mFsblPb.setVisibility(View.GONE);
                        mFsblContainer.setVisibility(View.VISIBLE);
                    }
                    if (smart_connector_flag == false) {
                        String noDeviceLabel = getResources().getString(R.string.device_not_found);
                        mDeviceStatusView.setText(noDeviceLabel);
                        mDeviceConnMode.setText("NA");
                        mSmartConnectorId.setText("--");

                    }

                } else {
                    String noDeviceLabel = getResources().getString(R.string.device_not_found);
                    mDeviceStatusView.setText(noDeviceLabel);
                    mDeviceConnMode.setText("NA");
                    mSmartConnectorId.setText("--");
                    mFsblStatusView.setText(noDeviceLabel);
                    mFsblConnMode.setText("NA");
                    mFsblId.setText("--");
                    mFsblPb.setVisibility(View.GONE);
                    mFsblContainer.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void failure(String msg) {
                msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
            }

            @Override
            public void onBefore() {
//                mDevicePb.setVisibility(View.VISIBLE);
//                mFsblPb.setVisibility(View.VISIBLE);
//                mFsblContainer.setVisibility(View.INVISIBLE);
//                mDeviceContainer.setVisibility(View.INVISIBLE);
                mFsblPb.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinish() {
                //Smart-connector
                mDevicePb.setVisibility(View.GONE);
                mDeviceContainer.setVisibility(View.VISIBLE);
                //FSBL
                mFsblPb.setVisibility(View.GONE);
                mFsblContainer.setVisibility(View.VISIBLE);
            }
        });
    }

    private void initialiseViews() {
        mRootView = (LinearLayout) findViewById(R.id.settings_root_view);
        mFsblContainer = (LinearLayout) findViewById(R.id.fsbl_detail_container);
        mDeviceContainer = (LinearLayout) findViewById(R.id.device_detail_container);
        mDevicePb = (ProgressBar) findViewById(R.id.devices_pb);
        mFsblPb = (ProgressBar) findViewById(R.id.fsbl_pb);
        mDeviceStatusView = (TypeFaceTextView) findViewById(R.id.device_online_status);
        mDeviceConnMode = (TypeFaceTextView) findViewById(R.id.device_mode);
        mFsblStatusView = (TypeFaceTextView) findViewById(R.id.fsbl_online_status);
        mFsblConnMode = (TypeFaceTextView) findViewById(R.id.fsbl_connection_mode);
        mFsblId = (TypeFaceTextView) findViewById(R.id.fsbl_device_id);
        mSmartConnectorId = (TypeFaceTextView) findViewById(R.id.smartConnector_device_id);

//        mDevicesListView = (RecyclerView) findViewById(R.id.device_status_lv);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(DevicesActivity.this);
//        mDevicesListView.setLayoutManager(linearLayoutManager);
//        mDevicesAdapter = new DevicesAdapter(DevicesActivity.this, devicesList);
//        mDevicesListView.setAdapter(mDevicesAdapter);
    }

    private void setUpActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.devices_toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_icon_white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
