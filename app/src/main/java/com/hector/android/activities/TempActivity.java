//package com.hector.android.activities;
//
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//
//import com.hector.android.R;
//import com.hector.android.mqtt.MqttActions;
//import com.hector.android.mqtt.MqttConstants;
//import com.hector.android.mqtt.MqttServerDetails;
//
//public class TempActivity extends AppCompatActivity {
//
//    private Button connectButton;
//    private Button publishButton;
//    private EditText publishMessage;
//    private EditText publishTopic;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_temp);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        initialiseViews();
//        bindEvents();
//    }
//
//    private void bindEvents() {
//        connectButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Bundle dataBundle = new Bundle();
//                dataBundle.putString(MqttConstants.server, MqttServerDetails.server);
//                dataBundle.putString(MqttConstants.port, MqttServerDetails.port);
//                dataBundle.putString(MqttConstants.clientId, MqttServerDetails.applicationClientId);
//                dataBundle.putInt(MqttConstants.action, MqttConstants.connect);
//                dataBundle.putBoolean(MqttConstants.cleanSession, MqttServerDetails.cleanSession);
//                dataBundle.putString(MqttConstants.message,
//                        MqttServerDetails.message);
//                dataBundle.putString(MqttConstants.topic, MqttServerDetails.topic);
//                dataBundle.putInt(MqttConstants.qos, MqttConstants.defaultQos);
//                dataBundle.putBoolean(MqttConstants.retained,
//                        MqttConstants.defaultRetained);
//
//                dataBundle.putString(MqttConstants.username,
//                        MqttServerDetails.applicationUsername);
//                dataBundle.putString(MqttConstants.password,
//                        MqttServerDetails.applicationPassword);
//
//                dataBundle.putInt(MqttConstants.timeout,
//                        MqttConstants.defaultTimeOut);
//                dataBundle.putInt(MqttConstants.keepalive,
//                        MqttConstants.defaultKeepAlive);
//                dataBundle.putBoolean(MqttConstants.ssl,
//                        MqttConstants.defaultSsl);
//                MqttActions.getInstance().connectAction(TempActivity.this, dataBundle);
//            }
//        });
//        publishButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                MqttActions.getInstance().publish(TempActivity.this, smartCommunicatorId, publishTopic.getText().toString().trim(),
//                        publishMessage.getText().toString().trim());
//            }
//        });
//    }
//
//    private void initialiseViews() {
//        connectButton = (Button) findViewById(R.id.connect_btn);
//        publishButton = (Button) findViewById(R.id.publish_btn);
//        publishTopic = (EditText) findViewById(R.id.publish_topic);
//        publishMessage = (EditText) findViewById(R.id.publish_message);
//    }
//
//}
