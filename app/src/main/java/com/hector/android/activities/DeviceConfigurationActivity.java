package com.hector.android.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.hector.android.R;
import com.hector.android.application.Hector;
import com.hector.android.application.NetworkStateReceivers;
import com.hector.android.fragments.DeviceConfigurationFragment;

public class DeviceConfigurationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_configuration);
        setUpActionBar();
        if(!Hector.isConnectedToInternet){
            NetworkStateReceivers.showNoConnectivityDialog(DeviceConfigurationActivity.this);
        }
        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.device_config_container, new DeviceConfigurationFragment())
                    .commit();
        }
    }

    private void setUpActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.device_configuration_toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.back_icon_white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_TITLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


//    public void connectAndConfigure(){
//        fetchPb.setVisibility(View.VISIBLE);
//        Bundle dataBundle = new Bundle();
//        dataBundle.putString(MqttConstants.server, MqttServerDetails.server);
//        dataBundle.putString(MqttConstants.port, MqttServerDetails.port);
//        dataBundle.putString(MqttConstants.clientId, MqttServerDetails.applicationClientId);
//        dataBundle.putInt(MqttConstants.action, MqttConstants.connect);
//        dataBundle.putBoolean(MqttConstants.cleanSession, MqttServerDetails.cleanSession);
//        dataBundle.putString(MqttConstants.message,
//                MqttServerDetails.message);
//        dataBundle.putString(MqttConstants.topic, MqttServerDetails.tLocalBroadcastManager.getInstance(DeviceConfigurationActivity.this).registerReceiver(publishStatusReceiver, new IntentFilter(MqttConstants.PUBLISH_STATUS_UPDATE));opic);
//        dataBundle.putInt(MqttConstants.qos, MqttConstants.defaultQos);
//        dataBundle.putBoolean(MqttConstants.retained,
//                MqttConstants.defaultRetained);
//
//        dataBundle.putString(MqttConstants.username,
//                MqttServerDetails.applicationUsername);
//        dataBundle.putString(MqttConstants.password,
//                MqttServerDetails.applicationPassword);
//
//        dataBundle.putInt(MqttConstants.timeout,
//                MqttConstants.defaultTimeOut);
//        dataBundle.putInt(MqttConstants.keepalive,
//                MqttConstants.defaultKeepAlive);
//        dataBundle.putBoolean(MqttConstants.ssl,
//                MqttConstants.defaultSsl);
//        registerConnectionReceiver();
//        MqttActions.getInstance().connectAction(Hector.applicationContext, dataBundle);
//    }
//
//    private void registerConnectionReceiver() {
//        LocalBroadcastManager.getInstance(DeviceConfigurationActivity.this).registerReceiver(connectionStatusReceiver, new IntentFilter(MqttConstants.CONNECTION_STATUS_UPDATE));
//    }
//    BroadcastReceiver connectionStatusReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if(intent.getIntExtra(MqttConstants.CONNECTION_STATUS_UPDATE, 0) == MqttConstants.CONNECTION_SUCCESS) {
////                Toast.makeText(DeviceConfigurationActivity.this, "Connected " + 1, Toast.LENGTH_SHORT).show();
//                smartCommunicatorId = "48d705c2c53f";
//                LocalBroadcastManager.getInstance(DeviceConfigurationActivity.this).registerReceiver(publishStatusReceiver, new IntentFilter(MqttConstants.PUBLISH_STATUS_UPDATE));
//                MqttActions.getInstance().publish(DeviceConfigurationActivity.this, smartCommunicatorId, MqttConstants.DEVICE_WIFI_CONFIG_TOPIC, messageToPublish);
//            } else {
//                fetchPb.setVisibility(View.GONE);
//                Toast.makeText(DeviceConfigurationActivity.this, "Connection failed", Toast.LENGTH_SHORT).show();
//            }
//
//        }
//    };
//
//    BroadcastReceiver publishStatusReceiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
//            if(intent.getIntExtra(MqttConstants.PUBLISH_STATUS_UPDATE, 0) == MqttConstants.PUBLISH_SUCCESS) {
//                Toast.makeText(DeviceConfigurationActivity.this, "Device configured", Toast.LENGTH_SHORT).show  ();
//                finish();
//                MqttActions.getInstance().disconnect(Hector.applicationContext);
//            } else {
//                Toast.makeText(DeviceConfigurationActivity.this, "PUBLISHED " + 0, Toast.LENGTH_SHORT).show();
//                fetchPb.setVisibility(View.GONE);
//            }
//
//        }
//    };
}

