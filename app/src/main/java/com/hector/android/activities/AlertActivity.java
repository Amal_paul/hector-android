package com.hector.android.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.hector.android.R;
import com.hector.android.adapters.ActivitiesAdapter;
import com.hector.android.adapters.NotificationAdapter;
import com.hector.android.application.Hector;
import com.hector.android.application.Utils;
import com.hector.android.http.Endpoints;
import com.hector.android.http.MyHttp;
import com.hector.android.http.MyHttpCallback;
import com.hector.android.libs.TypeFaceTextView;
import com.hector.android.pojo.responses.notificationResponse.NotificationResponse;
import com.hector.android.pojo.responses.notificationResponse.dayNotifications;
import com.hector.android.pojo.responses.patientactivities.Activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class AlertActivity extends AppCompatActivity {

    private String LOG_TAG = "AlertActivity";
    private RecyclerView mActivitiesLv;
    private TypeFaceTextView mNoDateLabel;
    private LinearLayoutManager linearLayoutManager;
    private NotificationAdapter mActivitiesAdapter;
    private RelativeLayout mRootView;
    private ProgressBar mLoadingPb;
    private String previous = "";

    private ArrayList<Activity> activities = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mRootView = (RelativeLayout) findViewById(R.id.alertsView);
        mNoDateLabel = (TypeFaceTextView) mRootView.findViewById(R.id.empty_data_ph);
        mLoadingPb = (ProgressBar) findViewById(R.id.alerts_load_pb);
        mActivitiesLv = (RecyclerView) findViewById(R.id.alerts_list_view);
        linearLayoutManager = new LinearLayoutManager(this);
        mActivitiesLv.setLayoutManager(linearLayoutManager);
        mActivitiesAdapter = new NotificationAdapter(this, activities, mActivitiesLv);
        mActivitiesLv.setAdapter(mActivitiesAdapter);

        if (Hector.isConnectedToInternet) {
            fetchNotification();
        }

    }

    private void fetchNotification() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = formatter.format(calendar.getTime());

        MyHttp.getInstance(this, Endpoints.getNotificationsUrl(formattedDate), MyHttp.GET, NotificationResponse.class)
                .setDefaultHeaders()
                .setHeaders(MyHttp.X_APP_ID, Utils.X_APP_ID)
                .setHeaders(MyHttp.AUTHORIZATION, Utils.AUTHORIZATION)
                .setHeaders(MyHttp.HEADER_ACCESS_TOKEN, Hector.apiToken)
                .send(new MyHttpCallback<NotificationResponse>() {
                    @Override
                    public void success(NotificationResponse data) {
                        mLoadingPb.setVisibility(View.GONE);

                        if (data.code == null) {
                            Log.i(LOG_TAG, "Activities request success " + data.toString());
                            activities.clear();
                            ArrayList<Activity> activities1 = extractActivityFromResponse(data.dayActivities);
                            if (!activities1.isEmpty()) {
                                activities.addAll(processResponse(activities1));
                                activities.add(new Activity(ActivitiesAdapter.VIEW_TYPE_END_OF_LIST));
                            } else {
                                mNoDateLabel.setVisibility(View.VISIBLE);
                            }
                            mActivitiesAdapter.notifyDataSetChanged();

                        } else {
                            Log.i(LOG_TAG, "Activities request error" + data.message);
                            String msg = data.message == null || data.message.isEmpty() ? getResources().getString(R.string.http_error) : data.message;
                            Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                        }
                    }

                    @Override
                    public void failure(String msg) {
                        Log.i(LOG_TAG, "Activities request failed " + msg);
                        mLoadingPb.setVisibility(View.GONE);
                        msg = msg == null || msg.isEmpty() ? getResources().getString(R.string.http_error) : msg;
                        Utils.showSnackBar(mRootView, msg, Snackbar.LENGTH_LONG);
                    }

                    @Override
                    public void onBefore() {
                        mLoadingPb.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onFinish() {

                    }
                });
    }

    private ArrayList<Activity> processResponse(ArrayList<Activity> activities) {
        ArrayList<Activity> activityArrayList = new ArrayList<>();
        for (Activity activity : activities) {
            if (activity.dateTime != null) {
                String timeStamp = Utils.getTimeStamp(activity.dateTime);  // was activity.createdAt
                if (!previous.equals(timeStamp)) {
                    previous = timeStamp;
                    activityArrayList.add(new Activity(ActivitiesAdapter.VIEW_TYPE_SECTION, timeStamp));
                }
                activityArrayList.add(activity);
            }
        }
        return activityArrayList;
    }

    private ArrayList<Activity> extractActivityFromResponse(ArrayList<dayNotifications> dayActivitiesArrayList) {
        ArrayList<Activity> activityArrayList = new ArrayList<>();

        for (int i = 0; i < dayActivitiesArrayList.size(); i++) {
            dayNotifications dayactivities = new dayNotifications();
            dayactivities = dayActivitiesArrayList.get(i);
            activityArrayList.addAll(dayactivities.activities);
        }
        return activityArrayList;
    }

}
