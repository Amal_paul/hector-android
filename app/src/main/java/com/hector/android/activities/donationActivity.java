package com.hector.android.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.GridView;

import com.hector.android.R;
import com.hector.android.adapters.donationOrg_Adapter;

import java.util.Arrays;
import java.util.List;

public class donationActivity extends AppCompatActivity {

    private GridView donationGridView;
    private Integer[] images = {R.drawable.alzheimers_association_logo, R.drawable.american_red_cross, R.drawable.americanheartassociation
            , R.drawable.humansocietylogo, R.drawable.bigbros_bigsis_logo, R.drawable.american_cancer_society_logo};
    private List<Integer> imageArraylist;
    private donationOrg_Adapter donationOrgAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        imageArraylist = Arrays.asList(images);
        donationGridView = (GridView) findViewById(R.id.donation_orgs);
        donationOrgAdapter = new donationOrg_Adapter(this, imageArraylist);
        donationGridView.setAdapter(donationOrgAdapter);

    }

}
